/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package citessercice;

import citesservice.CitesPortType;
import citesservice.CitesService;
import citesservice.PacientType;
import citesservice.ReservaType;
import citesservice.ServeiMedicType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author opengeek
 */
public class CitesServiceTest {
    
    public CitesServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void hello() {
        CitesService cs = new CitesService();
        CitesPortType cp = cs.getCitesPort();
        PacientType pt = new PacientType();
        pt.setDni("47945690M");
        pt.setNom("Marc");
        pt.setCognom1("Mauri");
        pt.setCognom2("Alloza");
        
        ServeiMedicType smt = new ServeiMedicType();
        smt.setNom("Radiografia");
        ReservaType rt = cp.demanarCita(pt, smt);
        
        assertEquals("47945690M", rt.getDelSolicitant().getDni() );
        assertEquals("Radiografia", rt.getPelServei().getNom() );

    }
}