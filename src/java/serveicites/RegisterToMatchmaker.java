/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package serveicites;

import MatchMakerClient.MatchMaker;
import MatchMakerClient.MatchMakerWSImpl;
import java.io.File;
import java.net.MalformedURLException;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author opengeek
 */
@Singleton
@Startup
public class RegisterToMatchmaker {
    
     @PostConstruct
    public void init() throws MalformedURLException {
       MatchMaker m = new MatchMaker();
       MatchMakerWSImpl mm = m.getMatchMakerPort();
       mm.mapDomainOntology("http://medicinaesportivaontologies.bitbucket.org/proveidordeserveis.owl",
               "http://medicinaesportivaontologies.bitbucket.org/proveidordeserveis.owl");
       mm.registerServiceByURL("http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_cites.owl");
    }
    
}
