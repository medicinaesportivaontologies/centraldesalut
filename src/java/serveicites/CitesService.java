/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package serveicites;

import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.FileManager;
import java.io.FileNotFoundException;
import java.io.InputStream;
import javax.jws.WebService;
import org.bitbucket.medicinaesportivaontologies.onto_serv.cites.Pacient;
import org.bitbucket.medicinaesportivaontologies.onto_serv.cites.ReservaType;
import org.bitbucket.medicinaesportivaontologies.onto_serv.cites.ServeiMedic;

/**
 *
 * @author opengeek
 */
@WebService(serviceName = "CitesService", portName = "CitesPort", endpointInterface = "org.bitbucket.medicinaesportivaontologies.onto_serv.cites.CitesPortType", targetNamespace = "http://medicinaesportivaontologies.bitbucket.org/onto-serv/Cites", wsdlLocation = "WEB-INF/wsdl/CitesService/medicinaesportivaontologies.bitbucket.org/wsdl/servei_cites.wsdl")
public class CitesService {

    public org.bitbucket.medicinaesportivaontologies.onto_serv.cites.ReservaType demanarCita(org.bitbucket.medicinaesportivaontologies.onto_serv.cites.PacientType pacient, org.bitbucket.medicinaesportivaontologies.onto_serv.cites.ServeiMedicType serveiMedic) throws FileNotFoundException {
        
        Pacient pacientLocal = new Pacient();
        pacientLocal.setPacientID(pacient.getPacientID() );
        
        ServeiMedic serveiLocal = new ServeiMedic();
        serveiLocal.setNom(serveiMedic.getNom() );
        
        ReservaType rT = new ReservaType();
        rT.setDelSolicitant(pacientLocal);   
        
        return rT;
    }
    
}
